const alertDeleteTag = (tag) => {
    const id = $(tag).data('id');
    swal({
            title: 'Xóa tag ?',
            text: 'Tag bị xoá sẽ bị gỡ khỏi bài viết',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/tag/delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá tag',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được tag, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertDeleteCategory = (category) => {
    const id = $(category).data('id');
    swal({
            title: 'Xóa chuyên mục ?',
            text: 'Các chuyên mục con sẽ bị xoá hoàn toàn và không thể khôi phục',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/category/delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá chuyên mục',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được chuyên mục, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertDeletePost = (post) => {
    const id = $(post).data('id');
    swal({
            title: 'Xóa bài viết ?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/post/delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá bài viết',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được bài viết, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertDeleteUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Xóa người dùng ?',
            text: 'Bài viết, chuyên mục và tag của người dùng sẽ bị xoá hoàn toàn và không thể khôi phục. Chuyên mục và tag đã được sử dụng trong bài viết sẽ được chuyển qua người quản trị',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/user/delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá người dùng',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được người dùng, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteTag = (tag) => {
    const id = $(tag).data('id');
    swal({
            title: 'Xóa tag hoàn toàn ?',
            text: 'Tag bị xoá sẽ không thể khôi phục',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/tag/bin/completed-delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá tag',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được tag, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteCategory = (category) => {
    const id = $(category).data('id');
    swal({
            title: 'Xóa chuyên mục hoàn toàn ?',
            text: 'Chuyên mục bị xoá hoàn toàn sẽ không thể khôi phục',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/category/bin/completed-delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá chuyên mục',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được chuyên mục, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeletePost = (post) => {
    const id = $(post).data('id');
    swal({
            title: 'Xóa bài viết hoàn toàn?',
            text: 'Bài viết bị xoá sẽ không thể khôi phục',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/post/bin/completed-delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá bài viết',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được bài viết, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Xóa người dùng hoàn toàn ?',
            text: 'Người dùng bị xoá sẽ không thể khôi phục',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        })
        .then((result) => {
            if (result.value) {
                fetch('/admin/user/bin/completed-delete', {
                        method: 'DELETE',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Đã xoá người dùng',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Lỗi',
                                text: 'Không xóa được người dùng, vui lòng thử lại',
                            });
                        }
                    });
            }
        });
};
const postImageMain = (file) => {
    $('#imageMain').attr('hidden', true);
    $('#loadImage').removeAttr('hidden');
    const fileData = $(file).prop('files')[0];
    const formData = new FormData();
    formData.append('file', fileData);
    $.ajax({
        url: '/admin/post/image',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
    }).done((result) => {
        $('#imageMain').val(result.location);
        $('#imageMain').removeAttr('hidden');
        $('#loadImage').attr('hidden', true);
        $("img[id='demo']").remove();
        $('#closeImg').removeAttr('hidden');
        $('#demoImg').append(`<img id="demo" src="${result.location}" alt="">`);
    });
};
$(document).ready(() => {
    $('.deleteTag').click(function handle() {
        const tag = this;
        alertDeleteTag(tag);
    });
    $('.deleteCategory').click(function handle() {
        const category = this;
        alertDeleteCategory(category);
    });
    $('.deletePost').click(function handle() {
        const post = this;
        alertDeletePost(post);
    });
    $('.deleteUser').click(function handle() {
        const user = this;
        alertDeleteUser(user);
    });
    $('.completedDeleteTag').click(function handle() {
        const tag = this;
        alertCompletedDeleteTag(tag);
    });
    $('.completedDeleteCategory').click(function handle() {
        const category = this;
        alertCompletedDeleteCategory(category);
    });
    $('.completedDeletePost').click(function handle() {
        const post = this;
        alertCompletedDeletePost(post);
    });
    $('.completedDeleteUser').click(function handle() {
        const user = this;
        alertCompletedDeleteUser(user);
    });
    // file
    $('#file').change(function handle() {
        const file = this;
        postImageMain(file);
    });
    $('#closeImg').click(function handle() {
        $('#imageMain').val('');
        $('#closeImg').attr('hidden', true);
    });
    if($('#imageMain').val()){
        const link = $('#imageMain').val();
        $("img[id='demo']").remove();
        $('#closeImg').removeAttr('hidden');
        $('#demoImg').append(`<img id="demo" src="${link}" alt="">`);
    }
    $('#imageMain').keyup(function handle() {
        const link = $('#imageMain').val();
        $("img[id='demo']").remove();
        $('#closeImg').removeAttr('hidden');
        $('#demoImg').append(`<img id="demo" src="${link}" alt="">`);
    })


});