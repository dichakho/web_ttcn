const express = require('express');
const multer = require('multer');
const ValidateHelpers = require('../../../helpers/validate');
const PostControllers = require('../controllers/admin');


const upload = multer({
    dest: 'public/tmp/file/',
});

const router = express.Router();

router.use(['/edit/:id', '/delete', '/bin/restore/:id', '/bin/completed-delete'], PostControllers.middlewareModify);

router.route('/add')
    .get(PostControllers.getAddPost)
    .post(ValidateHelpers.checkPost, PostControllers.addPost);
router.route('/edit/:id')
    .get(PostControllers.getEditPost)
    .post(ValidateHelpers.checkPost, PostControllers.editPost);
router.get(['/manage-by-author', '/manage-by-author/:page'], PostControllers.getPostsByAuthor);
router.get(['/manage', '/manage/:page'], PostControllers.getPosts);
router.delete('/delete', PostControllers.deletePost);

router.get(['/bin', '/bin/:page'], PostControllers.getPostsRecycleBin);
router.get('/bin/restore/:id', PostControllers.restorePost);
router.delete('/bin/completed-delete', PostControllers.completedDeletePost);
router.post('/image', upload.single('file'), PostControllers.addImageMain);

module.exports = router;
