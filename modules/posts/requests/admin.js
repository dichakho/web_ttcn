module.exports = {
    handleValidatePost: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
            case 'title':
                if (!errMsg.errTitle) errMsg.errTitle = e.msg;
                break;

            case 'description':
                if (!errMsg.errDescription) errMsg.errDescription = e.msg;
                break;

            case 'content':
                if (!errMsg.errContent) errMsg.errContent = e.msg;
                break;

            case 'grandparentId':
                if (!errMsg.errCategory) errMsg.errCategory = e.msg;
                break;
            case 'tags':
                if (!errMsg.errTags) errMsg.errTags = e.msg;
                break;
            }
        });

        return errMsg;
    },

    handleErrorAddPost: (body, errMsg, res) => {
        res.render('admin/body/post/add', {
            ...body, ...errMsg, active: 'post_add',
        });
    },

    renderAddPost: (body, res) => {
        res.render('admin/body/post/add', {
            ...body, active: 'post_add',
        });
    },

    handleErrorEditPost: (id, body, errMsg, res) => {
        res.render('admin/body/post/edit', {
            id, ...body, ...errMsg, active: 'post_manage',
        });
    },

    renderEditPost: (post, res) => {
        const id = post._id;
        const {
            title,
            description,
            content,
            grandparentCategoryList,
            parentCategoryList,
            childCategoryList,
            grandparentId,
            parentId,
            childId,
            tagList,
            tags,
            cover,
        } = post;
        res.render('admin/body/post/edit', {
            id,
            title,
            description,
            content,
            grandparentCategoryList,
            parentCategoryList,
            childCategoryList,
            grandparentId,
            parentId,
            childId,
            tagList,
            tags,
            cover,
            active: 'tag_manage',
        });
    },

    handleSuccessPosts: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/post/manage', {
                posts: args[0],
                pagination: args[1],
                active: 'post_manage',
                activeCheck: 'all',
            });
        }

        return args[0].redirect('/admin/post/manage');
    },
    handleDeletePosts: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/post/recycle_bin', {
                posts: args[0],
                pagination: args[1],
                active: 'post_manage',
                activeCheck: 'all',
            });
        }

        return args[0].redirect('/admin/post/manage');
    },
    handleSuccessPostsByAuthor: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/post/manage-by-author', {
                posts: args[0],
                pagination: args[1],
                active: 'post_manage',
                activeCheck: 'all',
            });
        }

        return args[0].redirect('/admin/post/manage-by-author');
    },
};
