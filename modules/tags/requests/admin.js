module.exports = {
    handleValidateTag: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
                case 'name':
                    if (!errMsg.errName) errMsg.errName = e.msg;
                    break;
            }
        });
        return errMsg;
    },
    handleSuccessTags: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/tag/manage', {
                tags: args[0],
                pagination: args[1],
                active: 'tag_manage',
            });
        }
        return args[0].redirect('/admin/tag/manage');
    },
    handleDeleteTag: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/tag/recycle_bin', {
                tags: args[0],
                pagination: args[1],
                active: 'tag_recycle_bin',
            });
        }
        return args[0].redirect('/admin/tag/bin');
    },
    handleErrorAddTag: (body, errMsg, res) => {
        res.render('admin/body/tag/add', {
            ...body,
            ...errMsg,
            active: 'tag_add',
        });
    },
    handleErrorEditTag: (id, body, errMsg, res) => {
        res.render('admin/body/tag/edit', {
            id,
            ...body,
            ...errMsg,
            active: 'tag_edit',
        });
    },
    renderEditTag: (tag, res) => {
        const { name } = tag;
        const id = tag._id;
        res.render('admin/body/tag/edit', {
            id,
            name,
            active: 'tag_manage',
        });
    },
};