const Tag = require('../../../models/tag');
const Post = require('../../../models/post');

module.exports = {
    saveTag: (body) => {
        const {
            name,
            author,
        } = body;
        const cTag = new Tag({
            name,
            author,
        });
        return cTag.save();
    },

    countDocuments: filter => Tag.countDocuments(filter),

    findTagById: id => Tag.findById(id).select('_id name url author'),
    findTagByName: name => Tag.findOne({
        name,
    }),
    findTagByUrl: url => Tag.findOne({
        url,
    }),

    updateTag: async (id, field) => {
        const dbTag = await Tag.findById(id);
        dbTag.set(field);
        return dbTag.save();
    },
    findPostsByTags: id => Post.find({
        tags: id,
    }).select('_id tags'),
    updatePost: (id, field) => Post.findByIdAndUpdate(id, field, {
        new: true,
    }),
    getTagsOnPage: (perPage, page) => Tag.find({
            deletedAt: null,
        })
        .select('name url createdAt')
        .sort({
            updatedAt: -1,
        })
        .populate('author', 'fullname')
        .skip((perPage * page) - perPage)
        .limit(perPage),
    getTagsOnBin: (perPage, page) => Tag.find({
            deletedAt: {
                $ne: null,
            },
        })
        .select('name url createdAt deletedAt')
        .populate('author', 'fullname')
        .sort({
            updatedAt: -1,
        })
        .skip((perPage * page) - perPage)
        .limit(perPage),
    deletedTag: id => Tag.findByIdAndRemove(id),
};