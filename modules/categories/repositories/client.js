const Category = require('../../../models/category');


module.exports = {
    getAllChildByUrl: async (url) => {
        const cate = await Category.findOne({ url });
        const childCate = await Category.find({ parentId: cate._id });
        const grandChildCate = await Category.find({ grandparentId: cate._id });
        const listCate = [];
        listCate.push(cate._id);
        childCate.forEach(element => listCate.push(element._id));
        grandChildCate.forEach(element => listCate.push(element._id));
        return listCate;
    },
};
