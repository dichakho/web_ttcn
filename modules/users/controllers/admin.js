const {
    validationResult,
} = require('express-validator/check');
const UserRepositories = require('../repositories/admin');
const UserRequests = require('../requests/admin');
const HandleResponse = require('../../../helpers/handleResponse');
const Config = require('../../../config/config');
const Pagination = require('../../../helpers/pagination');

module.exports = {
    editUser: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return (HandleResponse.handleErrorAdmin(500, res));
            }
            await UserRepositories.updateUser(id, body).then(() => {
                req.flash('success', 'Lưu người dùng thành công');
                return UserRequests.handleSuccessUsers('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    getUsers: async (req, res) => {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await UserRepositories.countDocuments({ deletedAt: null });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }
        const users = await UserRepositories.getUsersOnPage(perPage, page);
        const roleList = [
            { name: 'manager', displayName: 'Quản lí' },
            { name: 'poster', displayName: 'Người đăng bài' },
        ];
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/user/manage');
        return UserRequests.handleSuccessUsers('render', users, roleList, pagination, res);
    },
    getUsersRecycleBin: async (req, res) => {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await UserRepositories.countDocuments({ deletedAt: { $ne: null } });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }
        const users = await UserRepositories.getUsersOnBin(perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/user/bin');
        return UserRequests.handleDeleteUsers('render', users, pagination, res);
    },
    deleteUser: async (req, res) => {
        try {
            const { id } = req.body;
            await UserRepositories.deletePostsByAuthor(id);
            await UserRepositories.deleteCategoriesByAuthor(id);
            await UserRepositories.deleteTagsByAuthor(id);
            UserRepositories.updateUser(id, { deletedAt: Date.now() }).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                throw error;
            });
            req.flash('success', 'Xoá người dùng thành công');
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
    restoreUser: (req, res) => {
        try {
            const { id } = req.params;
            UserRepositories.updateUser(id, { deletedAt: null }).then(() => {
                req.flash('success', 'Khôi phục người dùng thành công');
                return UserRequests.handleSuccessUsers('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    getInactiveUsers: async (req, res) => {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await UserRepositories.countDocuments({ deletedAt: null });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }
        const users = await UserRepositories.getInactiveUsers(perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/user/pending');
        return UserRequests.handleInactiveUsers('render', users, pagination, res);
    },
    completedDeleteUser: (req, res) => {
        try {
            const { id } = req.body;
            req.flash('Xoá người dùng thành công');
            UserRepositories.deleteUser(id).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                throw error;
            });
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
    confirmUser: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            await UserRepositories.updateUser(id, { status: 'active' }).then(() => {
                req.flash('success', 'Xác nhận người dùng thành công');
                return UserRequests.handleSuccessUsers('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    suspendUser: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            await UserRepositories.updateUser(id, { status: 'suspended' }).then(() => {
                req.flash('success', 'Đã dừng hoạt động tài khoản');
                return UserRequests.handleSuccessUsers('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
};