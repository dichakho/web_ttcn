const express = require('express');
const UserControllers = require('../controllers/admin');
const ValidateHelpers = require('../../../helpers/validate');

const router = express.Router();

router.use((req, res, next) => {
    if (res.locals.cUser.role === 'admin') {
        return next();
    }
    return res.render('admin/body/error/403');
});
router.post('/edit/:id', ValidateHelpers.checkRole, UserControllers.editUser);
router.delete('/delete', UserControllers.deleteUser);
router.get(['/manage', '/manage/:page'], UserControllers.getUsers);
router.get(['/bin', '/bin/:page'], UserControllers.getUsersRecycleBin);
router.get('/bin/restore/:id', UserControllers.restoreUser);
router.delete('/bin/completed-delete', UserControllers.completedDeleteUser);
router.get(['/inactive', '/inactive/:page'], UserControllers.getInactiveUsers);
router.get('/confirm/:id', UserControllers.confirmUser);
router.get('/suspend/:id', UserControllers.suspendUser);

module.exports = router;