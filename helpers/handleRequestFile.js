const fs = require('fs');
const sharp = require('sharp');

sharp.cache(false);

module.exports = {
    resizeImage: async (path, width, height) => {
        try {
            await sharp(path)
                    .resize(width, height)
                    .toFile(`${path}-tmp`);

                fs.unlinkSync(path);
                const image = fs.createReadStream(`${path}-tmp`);

                return image.path;
        } catch (error) {
            return new Error(500);
        }
    },

    checkFile: (file, type) => {
        if (type === 'image' && /^image/i.test(file.mimetype)) return true;

        const format = ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'vsd', 'rar', 'zip', 'pdf'];
        const formatFile = file.originalname.slice(file.originalname.lastIndexOf('.') + 1);

        if (type === 'app' && format.indexOf(formatFile) !== -1) return true;

        fs.unlinkSync(file.path);

        return false;
    },

    checkSizeFile: (file) => {
        if (file.size <= 25000000) return true;

        fs.unlinkSync(file.path);

        return false;
    },

    checkFormat: (format) => {
        const office = ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'vsd'];
        const compressed = ['rar', 'zip'];

        if (office.indexOf(format) > -1) return 'office';

        if (compressed.indexOf(format) > -1) return 'compressed';

        return 'pdf';
    },
};
