const {
    check,
} = require('express-validator/check');
const getSlug = require('speakingurl');
const User = require('../models/user');
const Tag = require('../models/tag');
const Category = require('../models/category');
const Validate = require('./function');

const checkRegister = [
    check('fullname').trim().not().isEmpty()
    .withMessage('Tên không được để trống')
    .isLength({
        min: 5,
    })
    .withMessage('Độ dài tên quá ngắn')
    .isLength({
        max: 20,
    })
    .withMessage('Độ dài tên quá dài')
    .custom(value => Validate.checkFullname(value))
    .withMessage('Tên không hợp lệ'),

    check('username').trim().not().isEmpty()
    .withMessage('Username không được để trống')
    .isLength({
        min: 5,
        max: 20,
    })
    .withMessage('Độ dài username phải trong khoảng 5-20 ký tự')
    .trim()
    .custom(value => Validate.checkUsername(value))
    .withMessage('Username chỉ được chứa chữ, số và dấu "_"')
    .custom(value => User.findOne({
        username: value,
    }).then((user) => {
        if (user) {
            return Promise.reject(new Error('Tên đăng nhập đã tồn tại'));
        }
    })),

    check('email').trim().not().isEmpty()
    .withMessage('Email không được để trống')
    .isEmail()
    .withMessage('Email không hợp lệ')
    .custom(value => User.findOne({
        email: value,
    }).then((user) => {
        if (user) {
            return Promise.reject(new Error('Email đã tồn tại'));
        }
    })),

    check('phoneNumber').trim().not().isEmpty()
    .withMessage('Số điện thoại không được để trống')
    .custom(value => Validate.checkPhoneNumber(value))
    .withMessage('Số điện thoại không hợp lệ'),

    check('password').not().isEmpty().withMessage('Mật khẩu không được để trống')
    .isLength({
        min: 5,
        max: 30,
    })
    .withMessage('Mật khẩu phải từ 5-30 ký tự')
    .custom(value => Validate.checkPassword(value))
    .withMessage('Mật khẩu không được chứa dấu tiếng việt'),

    check('passwordConfirm').custom((value, {
        req,
    }) => {
        if (value === req.body.password) return true;
    })
    .withMessage('Mật khẩu không khớp'),

    check('termsAgree').not().isEmpty()
    .withMessage('Bạn chưa đồng ý với các điều khoản'),
];

const checkProfile = [
    check('avatar').trim().not().isEmpty()
    .withMessage('Avatar không được để trổng')
    .isURL()
    .withMessage('Avatar phải là một link ảnh'),

    check('fullname').trim().not().isEmpty()
    .withMessage('Tên không được để trống')
    .isLength({
        min: 5,
    })
    .withMessage('Độ dài tên quá ngắn')
    .isLength({
        max: 20,
    })
    .withMessage('Độ dài tên quá dài')
    .custom(value => Validate.checkFullname(value))
    .withMessage('Tên không hợp lệ'),

    check('email').trim().not().isEmpty()
    .withMessage('Email không được để trống')
    .isEmail()
    .withMessage('Email không hợp lệ'),

    check('phoneNumber').trim().not().isEmpty()
    .withMessage('Số điện thoại không được để trống')
    .custom(value => Validate.checkPhoneNumber(value))
    .withMessage('Số điện thoại không hợp lệ'),
];

const checkPassword = [
    check('password').not().isEmpty().withMessage('Mật khẩu không được để trống')
    .isLength({
        min: 5,
        max: 30,
    })
    .withMessage('Mật khẩu phải từ 5-30 ký tự')
    .custom(value => Validate.checkPassword(value))
    .withMessage('Mật khẩu không được chứa dấu tiếng việt'),

    check('passwordConfirm').custom((value, {
        req,
    }) => {
        if (value === req.body.password) return true;
    })
    .withMessage('Mật khẩu không khớp'),
];

const checkPost = [
    check('title').trim().not().isEmpty()
    .withMessage('Tiêu đề không được để trống')
    .isLength({
        min: 3,
    })
    .withMessage('Tiêu đề quá ngắn')
    .isLength({
        max: 200,
    })
    .withMessage('Tiêu đề quá dài'),
    check('content').trim().not().isEmpty()
    .withMessage('Nội dung không được để trống')
    .isLength({
        min: 5,
    })
    .withMessage('Nội dung quá ngắn'),
    check('grandparentId').custom((value) => {
        if (value === '0') return false;
        return true;
    })
    .withMessage('Chuyên mục không được để trống'),
    check('tags').not().isEmpty()
    .withMessage('Tag không được để trống'),
];

const checkCategory = [
    check('name').trim().not().isEmpty()
    .withMessage('Tên chuyên mục không được để trống')
    .custom(async (value) => {
        const url = getSlug(value, {
            lang: 'vn',
        });
        const cate = await Category.findOne({
            url,
        });
        if (cate) return Promise.reject(new Error('Chuyên mục đã tồn tại'));
    }),
];

const checkTag = [
    check('name').trim().not().isEmpty()
    .withMessage('Tên tag không được để trống')
    .isLength({
        min: 1,
    })
    .withMessage('Độ dài tag quá ngắn')
    .isLength({
        max: 50,
    })
    .withMessage('Độ dài tag quá dài')
    .custom(async (value) => {
        const url = getSlug(value, {
            lang: 'vn',
        });
        const tag = await Tag.findOne({
            url,
        });
        if (tag) return Promise.reject(new Error('Tag đã tồn tại'));
    }),
];

const checkRole = [
    check('role').trim().not().isEmpty()
    .withMessage('Vai trò không được để trống')
    .custom((value) => {
        if (value !== 'manager' && value !== 'poster') return false;
        return true;
    })
    .withMessage('Vui lòng không can thiệp vào mã nguồn của hệ thống'),
];

module.exports = {
    checkRegister,
    checkCategory,
    checkPost,
    checkTag,
    checkProfile,
    checkPassword,
    checkRole,
};