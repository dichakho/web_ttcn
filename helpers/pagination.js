module.exports = {
  renderPagination: (current, pages, url) => {
    let html = '';
    if (pages > 0) {
      html += `
            <div class="col-md-2">Trang ${current} / ${pages} </div>
            <div class="col-md-12 text-center">
                <ul class="pagination text-center">`;
    }
    if (Number(current) === 1) {
      html += `<li class="page-item disabled">
                <a class="page-link">Trang đầu</a>
              </li>`;
      html += `<li class="page-item disabled">
              <a class="page-link">&laquo;</a>
            </li>`;
    } else {
      const k = Number(current) - 1;
      html += `<li class="page-item">
                <a class="page-link" href="${url}">Trang đầu</a>
              </li>`;
      html += `<li class="page-item">
              <a class="page-link" href="${url}/${k}">&laquo;</a>
            </li>`;
    }
    let i = Number(current) > 2 ? Number(current) - 2 : 1;
    if (i !== 1) {
      html += '<li class="page-item"><a>...</a></li>';
    }
    for (; i <= Number(current) + 2 && i <= pages; i += 1) {
      if (i === Number(current)) {
        html += `<li class="page-item active"><a class="page-link">${i}</a></li>`;
      } else {
        html += `<li class="page-item"><a class="page-link" href="${url}/${i}">${i}</a></li>`;
      }
      if (i === Number(current) + 2 && i < pages - 2) {
        html += '<li class="page-item"><a class="page-link">...</a></li>';
      }
    }
    if (Number(current) < pages - 3) {
      for (i = pages - 1; i <= pages; i += 1) {
        html += `<li class="page-item"><a class="page-link" href="${url}/${i}">${i}</a></li>`;
      }
    }
    if (Number(current) === pages - 3) {
        html += `<li class="page-item"><a class="page-link" href="${url}/${pages}">${pages}</a></li>`;
    }
    if (Number(current) === Number(pages)) {
      html += '<li class="page-item disabled"><a class="page-link">&raquo;</a></li>';
      html += '<li class="page-item disabled"><a class="page-link">Trang cuối</a></li>';
    } else {
      const k = Number(current) + 1;
      html += `<li class="page-item"><a class="page-link" href="${url}/${k}">&raquo;</a></li>`;
      html += `<li class="page-item"><a class="page-link" href="${url}/${pages}">Trang cuối</a></li>`;
    }
    html += '</ul></div>';
    return html;
  },
};