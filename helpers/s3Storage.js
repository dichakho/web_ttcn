const aws = require('aws-sdk');
const fs = require('fs');
const path = require('path');
const S3Config = require('../config/config');

aws.config.update({
  secretAccessKey: S3Config.s3.secretAccessKey,
  accessKeyId: S3Config.s3.accessKeyId,
  region: S3Config.s3.region,
});

const s3 = new aws.S3();

const s3Upload = async (location, file) => {
    try {
        let folder;
        if (/^image/i.test(file.mimetype)) folder = 'images/image';
        else folder = 'files/file';

        const data = fs.readFileSync(location);

        const params = {
            Bucket: S3Config.s3.bucket,
            ACL: 'public-read',
            Key: `${folder}-${Date.now() + path.extname(file.originalname)}`,
            Body: data,
            ContentType: file.mimetype,
        };

        return new Promise((resolve, reject) => {
            s3.upload(params, (err, result) => {
                fs.unlinkSync(location);

                if (err) {
                    reject(err);
                }
                resolve(result.Location);
            });
        });
    } catch (error) {
        return new Error(500);
    }
};

module.exports = { s3Upload };
