const redis = require('redis');

const { promisify } = require('util');

const client = redis.createClient(process.env.CLI_PORT, process.env.CLI_HOST);
const getAsync = promisify(client.get).bind(client);
const Category = require('../models/category');

module.exports = {
    storeCategory: async () => {
        try {
            const mainCategory = await Category.find({ parentId: 0, deletedAt: null });
            const childCategory = await Category.find({ deletedAt: null, grandparentId: 0, parentId: { "$ne": '0'} });
            const grandChildCategory = await Category.find({ deletedAt: null, grandparentId: { "$ne": '0'} });
            const m1 = [];
            const m2 = [];
            const m3 = [];
            mainCategory.forEach((e) => {
               m1.push({ id: e.id, name: e.name, url: e.url });
            });
            childCategory.forEach((e) => {
                m2.push({ id: e.id, name: e.name, parentId: e.parentId, url: e.url });
             });
            grandChildCategory.forEach((e) => {
                m3.push({ id: e.id, name: e.name, parentId: e.parentId, url: e.url });
             });
            m1.forEach((e) => {
                e.child = [];
                m2.forEach((c1) => {
                    c1.child = [];
                    m3.forEach((c2) => {
                        if (c2.parentId === c1.id) {
                            c1.child.push(c2);
                        }
                    });
                    if (c1.parentId === e.id) {
                        e.child.push(c1);
                    }
                 });
            });
            const category = JSON.stringify(m1);
            client.set('category', category);
        } catch (error) {
            console.log(error);
        }
    },

    getRenderCategory: async () => {
        try {
            let category = await getAsync('category');
            category = JSON.parse(category);
            return category;
        } catch (error) {
            console.log(error);
        }
    },
};