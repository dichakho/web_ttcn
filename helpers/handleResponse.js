module.exports = {
    handleErrorAdmin: (code, res) => {
        switch (code) {
            case 403:
                return res.render('admin/body/response/403');
            case 404:
                return res.render('admin/body/error/404');
            case 500:
                return res.render('admin/body/error/500');
        }
    },

    handleErrorClient: (code, res) => {
        switch (code) {
            case 403:
                return res.render('client/response/403');
            case 404:
                return res.render('client/error/404');
            case 500:
                return res.render('client/error/500');
        }
    }
};