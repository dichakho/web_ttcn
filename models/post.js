const mongoose = require('mongoose');

const {
    Schema,
} = mongoose;

const getSlug = require('speakingurl');

const postSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    content: {
        type: String,
        required: true,
    },
    cover: {
        type: String,
        required: true,
        default: 'http://cdn.osxdaily.com/wp-content/uploads/2009/08/defaultdesktop.jpg',
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'users',
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'categories',
    },
    tags: [{
        type: Schema.Types.ObjectId,
        ref: 'tags',
    }],
    deletedAt: {
        type: Date,
        default: null,
    },
    views: {
        type: Number,
        default: 0,
    },
    url: {
        type: String,
        trim: true,
    },
}, {
    timestamps: true,
});

postSchema.pre('save', function createURL(next) {
    const post = this;
    if (!post.isModified('title')) return next();
    const url = getSlug(post.title, {
        lang: 'vn',
    });
    const month = (post.updatedAt.getMonth() + 1) < 10 ? `0${post.updatedAt.getMonth() + 1}` : post.updatedAt.getMonth() + 1;
    const date = post.updatedAt.getDate() < 10 ? `0${post.updatedAt.getDate()}` : post.updatedAt.getDate();
    const hours = post.updatedAt.getHours() < 10 ? `0${post.updatedAt.getHours()}` : post.updatedAt.getHours();
    const minutes = post.updatedAt.getMinutes() < 10 ? `0${post.updatedAt.getMinutes()}` : post.updatedAt.getMinutes();
    const seconds = post.updatedAt.getSeconds() < 10 ? `0${post.updatedAt.getSeconds()}` : post.updatedAt.getSeconds();
    const time = `${post.updatedAt.getFullYear()}${month}${date}${hours}${minutes}${seconds}`;
    post.url = url + time;
    next();
});

module.exports = mongoose.model('posts', postSchema);
