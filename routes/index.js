const express = require('express');


const routerAdmin = require('./admin');
const routerClient = require('./client');

const router = express.Router();


router.use('/admin', routerAdmin);
router.use('/', routerClient);

module.exports = router;
