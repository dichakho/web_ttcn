const express = require('express');
const postRouter = require('../modules/posts/routes/client');


const router = express.Router();

router.use(postRouter);

module.exports = router;
